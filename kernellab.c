/*
 * Kernellab
 */
#include <linux/types.h>
#include <linux/sched.h>
#include <linux/uaccess.h>
#include <linux/kernel.h>
#include <linux/kobject.h>
#include <linux/string.h>
#include <linux/sysfs.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>

#include "pidinfo.h"

/* Part I */
static ssize_t kcurrent_write(struct kobject *kobj, struct kobj_attribute *attr,
			 const char *buf, size_t count)
{	
	int *pid = (int*)(*((int*)buf));

	if (copy_to_user(pid, &current->pid, sizeof(int)))
		printk("copy_to_user failed\n");

  	return count;
}

static struct kobj_attribute kcurrent_attribute =
	__ATTR(kcurrent, 0220, NULL, kcurrent_write);


/* Part II */
static ssize_t pid_write(struct kobject *kobj, struct kobj_attribute *attr,
			 const char *buf, size_t count)
{	
	struct sysfs_message *message = kmalloc(sizeof(struct sysfs_message), GFP_USER);
	struct sysfs_message **user_message = (struct sysfs_message**)buf;
	struct pid_info *info = kmalloc(sizeof(struct pid_info), GFP_USER);
	struct task_struct *task_list;

	if (copy_from_user(message, *user_message, sizeof(struct sysfs_message)))
		printk("copy_from_user failed\n");

	for_each_process(task_list) {
		if (task_list->pid == message->pid) {
			info->pid = task_list->pid;
			strcpy(info->comm, task_list->comm);	
			info->state = task_list->state;
			break;
		}
	}

	if (copy_to_user(message->address, info, sizeof(struct pid_info)))
		printk("copy_to_user failed\n");

	kfree(message);
	kfree(info);

  	return count;
}

static struct kobj_attribute pid_attribute =
	__ATTR(pid, 0220, NULL, pid_write);

/* Setup  */
static struct attribute *attrs[] = {
	&kcurrent_attribute.attr,
	&pid_attribute.attr,
	NULL,
};
static struct attribute_group attr_group = {
	.attrs = attrs,
};

static struct kobject *kernellab_kobj;

static int __init kernellab_init(void)
{
	struct task_struct *task_list;
	int retval;

        printk("kernellab module INJECTED\n");

	for_each_process(task_list) {
		if (task_list->pid == 1) {
			printk("Init process: %s\n", task_list->comm);
			break;
		}
	}

	kernellab_kobj = kobject_create_and_add("kernellab", kernel_kobj);
	if (!kernellab_kobj)
		return -ENOMEM;

	retval = sysfs_create_group(kernellab_kobj, &attr_group);
	if (retval) 
		kobject_put(kernellab_kobj);
	return retval;
}

static void __exit kernellab_exit(void)

{
        printk("kernellab module UNLOADED\n");

	kobject_put(kernellab_kobj);
}

module_init(kernellab_init);
module_exit(kernellab_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Arnar Gauti Ingason <arnari13@ru.is");
MODULE_AUTHOR("Atli Guðlaugsson <atligud13@ru.is");
