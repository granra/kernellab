#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

#include "pidinfo.h"

/* Part I */
void run_current()
{
	int pid = 0;
	int *pid_p = &pid;
	int fp;

	if ((fp = open("/sys/kernel/kernellab/kcurrent", O_APPEND | O_WRONLY)) == -1) {
		printf("Can't open kcurrent\n");
		exit(1);
	}
	write(fp, &pid_p, sizeof(int*));
	close(fp);
	
        printf("Current PID: %d\n", pid);
}

/* Part II */
void run_pid()
{
	struct sysfs_message message, *message_p;
        struct pid_info info;
	
	message_p = &message;
	message.address = &info;
	
	/* get current pid to put in sysfs_message */
	int *pid_p = &message.pid;

	int fp = open("/sys/kernel/kernellab/kcurrent", O_APPEND | O_WRONLY);
	if (fp == -1) {
		printf("Can't open kcurrent\n");
		exit(1);
	}
	write(fp, &pid_p, sizeof(int*));
	close(fp);

	/* ged pid_info */
	if ((fp = open("/sys/kernel/kernellab/pid", O_APPEND | O_WRONLY)) == -1) {
		printf("Can't open pid\n");
		exit(1);
	}
	write(fp, &message_p, sizeof(struct sysfs_message*));
	close(fp);

        printf("PID: %d\n", info.pid);
        printf("COMM: %s\n", info.comm);
        printf("State: %ld", info.state);
}


int main()
{
        run_current();    
        run_pid();
        return EXIT_SUCCESS;
}
